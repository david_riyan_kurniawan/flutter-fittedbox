import 'dart:ui';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

///widget ini berguna untuk meresponsivkan content yang ada di
///dalam widget fittedbox agar content bersifat adaptive
///ini berlaku untuk text,image dll.
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('fittedbox'),
        ),
        body: Center(
            child: Container(
          width: 150,
          height: 50,
          color: Colors.blueAccent,

          ///dengan begini maka semua widget yang dibungkus oleh fittedbox
          ///akan bersifat responsive atau adaptive
          ///didalam fittedbox terdapat berbagai macam component seperti
          ///fit, alignment, dll
          child: FittedBox(
            alignment: Alignment.centerLeft,
            child: Image.network(
                'https://th.bing.com/th/id/OIP.Ah1mh01UoZ6aIPUdZi5fhwHaEn?pid=ImgDet&rs=1'),
          ),
        )),
      ),
    );
  }
}
